package myprojects.automation.webinar.tests;

import myprojects.automation.webinar.tests.utils.BaseTest;
import myprojects.automation.webinar.tests.utils.WebDriverLogger;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.File;

public class SomeTest2 {
    WebDriver driver;

    @Parameters("browser")
    @BeforeClass
    public void beforeClass(String browser) {
        if (browser.equalsIgnoreCase("firefox")) {
            System.setProperty(
                    "webdriver.gecko.driver",
                    new File(BaseTest.class.getResource("/geckodriver.exe").getFile()).getPath());
            driver = new FirefoxDriver();

        } else if (browser.equalsIgnoreCase("ie")) {
            System.setProperty("webdriver.ie.driver",
                    new File(BaseTest.class.getResource("/IEDriverServer.exe").getFile()).getPath());
            driver = new InternetExplorerDriver();

        } else if (browser.equalsIgnoreCase("chrome")) {
            System.setProperty(
                    "webdriver.chrome.driver",
                    new File(BaseTest.class.getResource("/chromedriver.exe").getFile()).getPath());
            driver = new ChromeDriver();

        } else {
        }
        EventFiringWebDriver wrappedDriver = new EventFiringWebDriver(driver);
        driver = wrappedDriver.register(new WebDriverLogger());
    }


//    @BeforeClass
//    public void setup() {
//        System.setProperty(
//                "webdriver.chrome.driver",
//                new File(SomeTest.class.getResource("/chromedriver.exe").getFile()).getPath());
//        driver = new ChromeDriver();
//    }


    @AfterClass
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void loginTest() {
        driver.navigate().to("http://prestashop-automation.qatestlab.com.ua");
        driver.manage().window().maximize();
    }

    @Test(dependsOnMethods = "loginTest")
    public void allItemsPage() {
        WebElement allItemsButton = driver.findElement(By.xpath("//a[text()[contains(.,'Все товары')]]"));
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].scrollIntoView(true);", allItemsButton);
        driver.findElement(By.xpath("//a[text()[contains(.,'Все товары')]]")).click();
    }

    @Test(dependsOnMethods = "allItemsPage")
    public void addedItemAssertion() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@placeholder='Поиск в каталоге']")));
        driver.findElement(By.xpath("//input[@placeholder='Поиск в каталоге']")).sendKeys(SomeTest.getItemName() + (Keys.ENTER));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id ='js-product-list']//article[last()]")));
        WebElement lastItemInList = driver.findElement(By.xpath("//div[@id ='js-product-list']//article[last()]"));
        //JavascriptExecutor executor = (JavascriptExecutor) driver;
        //executor.executeScript("arguments[0].scrollIntoView(true);", newI);
        // wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//article[last()]/div/div[1]/h1/a[text()='\" + SomeTest.getItemName() + \"']/ancestor::article")));
        WebElement lastItemContainsName = driver.findElement(By.xpath("//article[last()]/div/div[1]/h1/a[text()='" + SomeTest.getItemName() + "']/ancestor::article"));
        System.out.println(lastItemContainsName);
        WebElement lastItemContainsPrice = driver.findElement(By.xpath("//span[text()[contains(.,'" + SomeTest.getItemsPrice().replace(".", ",") + "')]]/ancestor::article[last()]"));

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(lastItemInList, lastItemContainsName);
        softAssert.assertEquals(lastItemInList, lastItemContainsPrice);
        softAssert.assertAll();

    }

    @Test(dependsOnMethods = "addedItemAssertion")
    public void addedItemParametersAssertion() {
        //WebElement lastItemInList = driver.findElement(By.xpath("//div[@id ='js-product-list']//article[last()]"));
        driver.findElement(By.xpath("//*[@id=\"js-product-list\"]/div[1]/article[last()]/div/div[1]/h1/a")).click();
        WebDriverWait wait = new WebDriverWait(driver, 5);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[1]")));

        SoftAssert softAssert = new SoftAssert();
        //assertName
        softAssert.assertEquals(driver.findElement(By.xpath("//h1[1]")), driver.findElement(By.xpath("//h1[text()='" + SomeTest.getItemName() + "']")));
        //assertPrice
        softAssert.assertEquals(driver.findElement(By.xpath("//*[@class='current-price']/span")), driver.findElement(By.xpath("//span[@content = '" + SomeTest.getItemsPrice() + "']")));

        //assertCount
        softAssert.assertEquals(driver.findElement(By.xpath("//*[@id=\"product-details\"]/div[1]/span")), driver.findElement(By.xpath("//span[text()='" + SomeTest.getItemCount() + " Товары']")));
        softAssert.assertAll();

    }
}
